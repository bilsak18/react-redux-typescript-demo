/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Wednesday November 25th 2020
 */


describe('The home page', () => {
    it('navigates to the login form', () => {
        cy.visit('/')

        cy.get('[data-test=loginNav]')
        .click()
        .url()
        .should('match', /login/)

        cy.contains('Login with your credentials')
    })

    it('navigates to the posts list', () => {
        cy.visit('/login')

        cy.get('[data-test=postsNav]')
        .click()
        .url()
        .should('equal', Cypress.config().baseUrl + '/')

        cy.contains('Posts')
    })
})