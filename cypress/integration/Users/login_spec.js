/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 23rd 2020
 */


describe('The login page', () => {

    before(() => {
        cy.visit('/login')
    })

    beforeEach(() => {
        cy.get('input[data-test=username]')
        .clear()
        .type('test')

        cy.get('input[data-test=password]')
        .clear()
        .type('testtest')
    })

    it('displays wrong password error', () => {
        // Attempt to login with an incorrect password
        cy.get('input[data-test=password]')
        .clear()
        .type('wrongpassword')

        cy.get('button[data-test=loginButton]')
        .click()

        cy.get('[data-test=errorMessage]')
        .contains('Wrong password')
    })

    it('logs in correctly with an existing user', () => {
        // Click login button
        cy.get('button[data-test=loginButton]')
        .click()

        // Check if user was redirected to home page after login button click
        cy.url()
        .should('eq', Cypress.config().baseUrl + '/')

        // Check if user was logged in correctly
        cy.visit('/login')

        cy.get('[data-test=loginInfo]')
        .should('contain', 'logged in as Test User')
        .and(() => {
            // Check if localStorage currentUser item was set correctly
            expect(JSON.parse(localStorage.getItem('currentUser'))).to.deep.include({username: 'test'})
        })
    })
})