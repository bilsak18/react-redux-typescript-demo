/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 24th 2020
 */


describe('The login page', () => {
    before(() => {
        cy.visit('/login')
    })

    beforeEach(() => {
        cy.get('input[data-test=username]')
        .clear()
        .type('Bill')
 
        cy.get('input[data-test=password]')
        .clear()
        .type('passpass')
    })

    it('logs in with a new user', function() {
        cy.get('button[data-test=loginButton]')
        .click()

        // Check if user was redirected to home page after login button click
        cy.url()
        .should('eq', Cypress.config().baseUrl + '/')

        // Check if user was logged in correctly
        cy.visit('/login')

        cy.get('[data-test=loginInfo]')
        .should('contain', 'logged in as Bill')
        .and(() => {
            // Check if localStorage currentUser item was set correctly
            expect(JSON.parse(localStorage.getItem('currentUser'))).to.deep.include({username: 'Bill'})
        })
    })
})