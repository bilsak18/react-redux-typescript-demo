/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 17th 2020
 */


describe("The add post form", () => {

	before(() => {
		cy.visit('/')
	})

	beforeEach(() => {
		cy.get('input[data-test=postTitle]')
		.clear()
		.type('Post title with cypress')

		cy.get('select[data-test=postAuthor]')
		.select('Test User')
		.should('have.value', 1)

		cy.get('textarea[data-test=postContent]')
		.clear()
		.type('Post content with cypress')
	})

	context('ideal workflow', () => {
		it("adds a post correctly to the posts list", () => {
			cy.get('button[data-test=saveButton]')
			.click()

			cy.get('article[data-test=post]')
			.should('contain', 'Post title with cypress')
			.should('contain', 'Post content with cypress')
			.and('contain', 'Test User')
		})
	})

	context("Empty field error handling workflow", () => {
		it("displays error for Post Title input", () => {
			cy.get('input[data-test=postTitle]')
			.clear()
			.type('     ')

			cy.get('button[data-test=saveButton]')
			.click()

			cy.get('[data-test=errorMessage]')
			.should('exist')
		})

		it("displays error for content textarea", () => {
			cy.get('textarea[data-test=postContent]')
			.clear()
			.type('     ')

			cy.get('button[data-test=saveButton]')
			.click()

			cy.get('[data-test=errorMessage]')
			.should('exist')
		})

		it("displays error for both fields", () => {
			cy.get('input[data-test=postTitle]')
			.clear()
			.type('     ')

			cy.get('textarea[data-test=postContent]')
			.clear()
			.type('     ')

			cy.get('button[data-test=saveButton]')
			.click()

			cy.get('[data-test=errorMessage]')
			.should('exist')
		})
	})

	
	
})