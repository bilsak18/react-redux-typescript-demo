/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday November 19th 2020
 */


describe("The single post page", () => {

	before(() => {
		cy.visit('/')
	})

	it("views a single post", () => {
		cy.get("article[data-test=post]")
		.should("have.length.above", 0)

		cy.get("[data-test=viewPostLink]")
		.first()
		.click()
		.url()
		.should('match', /posts\/\S+/)

		// Now we are on Single Post Page
	})

	it("Edits a post", () => {
		cy.get("[data-test=editPostLink]")
		.click()
		.url()
		.should('match', /editPost\/\S+/)

		// Now we are on Edit Post Page

		cy.get('input[data-test=postTitle]')
		.should('have.length.gt', 0)
		.clear()
		.type('A post title updated with Cypress')

		cy.get('textarea[data-test=postContent]')
		.should('have.length.gt', 0)
		.clear()
		.type('A post content updated with Cypress')

		cy.get('button[data-test=savePostButton]')
		.click()
		.url()
		.should('match', /posts\/\S+/)

		// Now we are back to the Single Post Page
	})

	it("checks if post was updated correctly", () => {
		cy.get('article[data-test=singlePost]')
		.should('contain', 'A post title updated with Cypress')
		.and('contain', 'A post content updated with Cypress')
	})


})