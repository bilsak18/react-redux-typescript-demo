/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Wednesday November 25th 2020
 */


describe('The posts list', () => {
    before(() => {
        cy.visit('/')
    })
    
    it('adds a reaction to a post from the posts list', function() {
        cy.get('article[data-test=post]')
        .contains('Test title')
        .parent()
        .as('post')

        cy.get('@post')
        .within(post => {
            cy.get('[data-test=reactionsDiv] > button')
            .eq(2) // heart reaction
            .as('heartButton')

            cy.get('@heartButton')
            .should('contain','0')

            cy.get('@heartButton')
            .click()

            cy.get('@heartButton')
            .should('contain','1')
        })
    })
})