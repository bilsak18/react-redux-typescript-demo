/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import { LoginForm } from 'features/users/loginForm';
import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'

import { Navbar } from './app/Navbar';
import { AddPostForm } from './features/posts/AddPostForm';
import { EditPostForm } from './features/posts/EditPostForm';
import { PostsList } from './features/posts/postsList';
import { SinglePostPage } from './features/posts/SinglePostPage'


function App() {
  return (
    <Router>
      <Navbar />
      <div className="App">
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
				<React.Fragment>
					<AddPostForm />
					<PostsList />
				</React.Fragment>
            )}
          />
		  <Route exact path="/posts/:postId" component={SinglePostPage} />
		  <Route exact path="/editPost/:postId" component={EditPostForm} />
		  <Route exact path="/login" component={LoginForm} />
          <Redirect to="/" />
        </Switch>
      </div>
    </Router>
  )
}

export default App
