/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React from 'react'
import { Link } from 'react-router-dom'

export const Navbar = () => {
  return (
    <nav>
      <section>
        <h1>Redux Essentials Example</h1>

        <div className="navContent">
			<div className="navLinks">
              <Link to="/login" data-test="loginNav">Authentication</Link>
            	<Link to="/" data-test="postsNav">Posts</Link>
          	</div>
        </div>
      </section>
    </nav>
  )
}
