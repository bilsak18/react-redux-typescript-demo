/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 9th 2020
 */


import { configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import postsReducer from '../features/posts/postsSlice';
import usersReducer from '../features/users/usersSlice';

const store = configureStore({
  	reducer: {
		posts: postsReducer,
	  	users: usersReducer
  	},
})

export default store;

export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()
export type RootState = ReturnType<typeof store.getState>