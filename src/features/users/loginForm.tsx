/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 23rd 2020
 */

import { unwrapResult } from "@reduxjs/toolkit"
import { AppDispatch, RootState, useAppDispatch } from "app/store"
import React, { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { User } from "./types"
import { addNewUser } from "./usersSlice"


export const LoginForm = () => {


    const dispatch: AppDispatch = useAppDispatch()
    const history = useHistory()

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [currentUser, setCurrentUser] = useState<null | User >(null)
    const [wrongPassword, setWrongPassword] = useState(false)

    const users = useSelector((state: RootState) => state.users.users)

    useEffect(() => {
        const local = JSON.parse(localStorage.getItem('currentUser') as string)
        if(local) {
            setCurrentUser(local)
        }
    }, [setCurrentUser])

    const onUsernameChanged = (e: any) => {
        setWrongPassword(false)
        setUsername(e.target.value)
    }

    const onPasswordChanged = (e: any) => {
        setPassword(e.target.value)
    }

    const canLogin = Boolean(username) && Boolean(password) && password.length > 7

    const onLoginClick = async () => {
        if(canLogin) {
            const existingUser = users.find(user => user.username === username)

            if(!existingUser) {
                // Added unexisting user to the users list and authenticate them directly
                try {
                    const resultAction = await dispatch(addNewUser({name: username, username, password} as User))
                    unwrapResult(resultAction)
                    localStorage.setItem('currentUser', JSON.stringify(resultAction.payload))
                } catch(error) {
                    console.log(error)
                } finally {
                    setWrongPassword(false)
                    history.push('/')
                }
            } else {
                if(existingUser.password === password) {
                    // Correct password for existing user, authenticate
                    localStorage.setItem('currentUser', JSON.stringify(existingUser))
                    setWrongPassword(false)
                    history.push('/')
                } else {
                    // Wrong password for existing user, show error
                    setWrongPassword(true)
                    setPassword('')
                }
            }
        }
    }

    const onLogoutClick = () => {
        localStorage.removeItem('currentUser')
        setCurrentUser(null)
        history.push('/login')
    }

    let errorMessage =
            <h4 className="error" data-test="errorMessage">Wrong password for existing user: {username}</h4>

    let content

    if(currentUser) {
        // Show currentUser info
        content = 
            <div>
                <h2 data-test="loginInfo">You are currently logged in as {currentUser.name}</h2>
                <button
                    name="logoutButton"
                    type="button"
                    onClick={onLogoutClick}
                    data-test="logoutButton"
                >
                    LOGOUT
                </button>
            </div>

    } else {
        // Show login form
        content = 
            <>
                <h2>Login with your credentials</h2>
                <form>
                    <label htmlFor="username">Username: </label>
                    <input
                        type="text"
                        id="username"
                        name="username"
                        value={username}
                        onChange={onUsernameChanged}
                        data-test="username" />
                    <label htmlFor="password">Password: </label>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        value={password}
                        onChange={onPasswordChanged}
                        data-test="password" />
                        {wrongPassword ? errorMessage : null}
                    <button
                        name="loginButton"
                        type="button"
                        onClick={onLoginClick}
                        disabled={!canLogin}
                        data-test="loginButton"
                    >
                        LOGIN
                    </button>
                </form>
            </>
    }

    return (
        <section>
            {content}
        </section>
    )
}