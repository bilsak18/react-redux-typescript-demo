/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 9th 2020
 */


import { createAsyncThunk, createSlice, nanoid, PayloadAction } from '@reduxjs/toolkit'
import { client } from '../../api/client'
import { UserState, User } from './types'

const initialState: UserState = {
	users: []
}

export const fetchUsers = createAsyncThunk('users/fetchUsers', async () => {
	const response = await client.get('fakeApi/users')
	return response.users
})

export const addNewUser = createAsyncThunk('users/addNewUser', async (newUser: User) => {
	const response = await client.post('fakeApi/users', {user: newUser})
	return response.user
})

const usersSlice = createSlice({
	name: 'users',
	initialState,
	reducers: {
		userAdded: {
			reducer(state, action: PayloadAction<User>) {
				state.users.push(action.payload)
			},
			prepare(username: string, password: string) {
				return {
					payload: {
						id: nanoid(),
						name: username,
						username,
						password
					}
				}
			}
		}
	},
	extraReducers: builder => {
		builder.addCase(fetchUsers.fulfilled, (state, action) => {
			state.users = state.users.concat(action.payload)
		})
		builder.addCase(addNewUser.fulfilled, (state, action) => {
			state.users.push(action.payload)
		})
	}
});

export const {userAdded} = usersSlice.actions

export default usersSlice.reducer;