/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 9th 2020
 */


export interface UserState {
	users : User[]
}

export interface User {
	id: string,
	name: string,
	username: string,
	password: string
}