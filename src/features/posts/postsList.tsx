/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { fetchPosts, selectAllPosts } from './postsSlice'
import { PostAuthor } from './PostAuthor'
import { ReactionButtons } from './ReactionButtons'
import { AppDispatch, useAppDispatch, RootState } from '../../app/store'

export const PostsList = () => {
	const dispatch: AppDispatch = useAppDispatch()
	const posts = useSelector(selectAllPosts);

	const postStatus = useSelector((state: RootState) => state.posts.status)
	const error = useSelector((state: RootState) => state.posts.error) 

	useEffect(() => {
		if(postStatus === 'idle') {
			dispatch(fetchPosts())
		}
	}, [postStatus, dispatch])

	let content;

	if(postStatus === 'loading') {
		content = <div className='loader'>Loading...</div>
	} else if(postStatus === 'complete') {
		content = posts.map(post => (
			<article className="post-excerpt" key={post.id} data-test="post">
				<h3>{post.title}</h3>
				<PostAuthor userId={post.user} />
				<p>{post.content.substring(0, 100)}</p>
				<ReactionButtons post={post} />
				<Link to={`/posts/${post.id}`} className="button muted-button" data-test="viewPostLink">
					View Post
				</Link>
			</article>
		))
	} else if (postStatus === 'failed') {
		content = <div>{error}</div>
	}

	return (
		<section>
			<h2>Posts</h2>
			{content}
		</section>
	);
}