/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { RootState } from '../../app/store'

import { postUpdated, selectPostById } from './postsSlice'
import { Post } from './types'

export const EditPostForm = ({ match } : {match: any}) => {
  const { postId } : { postId: string }= match.params

  const post: Post = useSelector((state: RootState) => selectPostById(state, postId))

  const [title, setTitle] = useState(post.title)
  const [content, setContent] = useState(post.content)

  const dispatch = useDispatch()
  const history = useHistory()

  const onTitleChanged = (e: any) => setTitle(e.target.value)
  const onContentChanged = (e: any) => setContent(e.target.value)

  const onSavePostClicked = () => {
    if (title && content) {
      dispatch(postUpdated({ id: postId, title, content }))
      history.push(`/posts/${postId}`)
    }
  }

  return (
    <section>
      <h2>Edit Post</h2>
      <form>
        <label htmlFor="postTitle">Post Title:</label>
        <input
          type="text"
          id="postTitle"
          name="postTitle"
          placeholder="What's on your mind?"
          value={title}
		  onChange={onTitleChanged}
		  data-test="postTitle"
        />
        <label htmlFor="postContent">Content:</label>
        <textarea
          id="postContent"
          name="postContent"
          value={content}
		  onChange={onContentChanged}
		  data-test="postContent"
        />
      </form>
      <button type="button" onClick={onSavePostClicked} data-test="savePostButton">
        Save Post
      </button>
    </section>
  )
}