/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { PostAuthor } from './PostAuthor'
import { ReactionButtons } from './ReactionButtons'
import { selectPostById } from './postsSlice'
import { RootState } from '../../app/store'

export const SinglePostPage = ({ match } : {match: any}) => {
  const { postId } = match.params

  const post = useSelector((state: RootState) => selectPostById(state, postId));

  if (!post) {
    return (
      <section>
        <h2>Post not found!</h2>
      </section>
    )
  }

  return (
    <section>
      <article className="post" data-test='singlePost'>
        <h2>{post.title}</h2>
		    <PostAuthor userId={post.user} />
        <p className="post-content">{post.content}</p>
		    <ReactionButtons post={post} />
		    <Link to={`/editPost/${post.id}`} className="button" data-test="editPostLink">
          Edit Post
        </Link>
      </article>
    </section>
  )
}