/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React from 'react'
import { useDispatch } from 'react-redux'

import { reactionAdded } from './postsSlice'
import { Post, Reactions } from './types'

const reactionEmoji = {
	thumbsUp: '👍',
	hooray: '🎉',
	heart: '❤️',
	rocket: '🚀',
	eyes: '👀'
}

export const ReactionButtons = ({ post } : {post: Post}) => {
	const dispatch = useDispatch()

	const reactionButtons = Object.entries(reactionEmoji).map(([name, emoji]) => {
		return (
		<button key={name} type="button" className="muted-button reaction-button"
			onClick={() =>
				dispatch(reactionAdded({ postId: post.id, reaction: name }))
			}>
			{emoji} {post.reactions[name as keyof Reactions]}
		</button>
		)
	})

	return <div data-test="reactionsDiv">{reactionButtons}</div>
}