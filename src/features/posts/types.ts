/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 9th 2020
 */


export interface Post {
	id: string
	title: string
	content: string
	user: string
	reactions: Reactions
}

export interface PostState {
	posts: Post[]
	status: 'idle' | 'loading' | 'complete' | 'failed'
	error: string
}

export interface Reactions {
	thumbsUp: number
	hooray: number
	heart: number
	rocket: number
	eyes: number
}