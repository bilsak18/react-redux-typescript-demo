/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import { unwrapResult } from '@reduxjs/toolkit'
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { AppDispatch, RootState, useAppDispatch } from '../../app/store'
import { addNewPost } from './postsSlice'
import { Post } from './types'

export const AddPostForm = () => {
	const [title, setTitle] = useState('')
	const [content, setContent] = useState('')
	const [userId, setUserId] = useState('')
	const [addRequestStatus, setAddRequestStatus] = useState('idle')
	const [isWhiteSpace, setIsWhiteSpace] = useState(false)


	const onTitleChanged = (e: any) => setTitle(e.target.value)
	const onContentChanged = (e: any) => setContent(e.target.value)
	const onAuthorChanged = (e: any) => setUserId(e.target.value)

	const dispatch: AppDispatch = useAppDispatch();

	const users = useSelector((state: RootState) => state.users.users)

	const canSave = Boolean(title) && Boolean(content) && Boolean(userId) && addRequestStatus === 'idle'

	const onSavePostClick = async () => {
		if(canSave) {
			if(title.match(/^\s+$/) || content.match(/^\s+$/)) {
				setIsWhiteSpace(true)
				return;			
			}

			try {
				setAddRequestStatus('loading')
				const resultAction = await dispatch(addNewPost({title, content, user: userId} as Post))
				unwrapResult(resultAction)
				setTitle('')
				setContent('')
				setUserId('')
			} catch(err) {
				console.log('Failed to save the post: ', err)
			} finally {
				setAddRequestStatus('idle')
				setIsWhiteSpace(false)
			}
		}
	}

	const usersOptions = users.map(user => (
		<option key={user.id} value={user.id}>
			{user.name}
		</option>
	))

	let errorMessage = 
		<h2 className="error" data-test="errorMessage">
			Post title or content cannot be empty
		</h2>

	return (
		<section>
		<h2>Add a New Post</h2>
		{isWhiteSpace ? errorMessage : null}
		<form>
			<label htmlFor="postTitle">Post Title:</label>
			<input
				type="text"
				id="postTitle"
				name="postTitle"
				value={title}
				onChange={onTitleChanged}
				data-test="postTitle"
			/>
			<label htmlFor="postAuthor">Author:</label>
			<select id="postAuthor" value={userId} onChange={onAuthorChanged} data-test="postAuthor">
				<option value=""></option>
				{usersOptions}
			</select>
			<label htmlFor="postContent">Content:</label>
			<textarea
				id="postContent"
				name="postContent"
				value={content}
				onChange={onContentChanged}
				data-test="postContent"
			/>
			<button 
				name="saveButton"
				type="button"
				onClick={onSavePostClick}
				disabled={!canSave}
				data-test="saveButton"
			>
				Save Post
			</button>
		</form>
		</section>
	)
}