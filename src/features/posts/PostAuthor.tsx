/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React from 'react'
import { useSelector } from "react-redux"
import { RootState } from '../../app/store'


export const PostAuthor = ({userId} : {userId: string}) => {
	const author = useSelector((state: RootState) => 
		state.users.users.find(user => user.id === userId)
	)

	return (
		<span>
			by {author ? author.name : 'Unkown author'}
		</span>
	)
}