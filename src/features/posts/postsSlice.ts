/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday November 9th 2020
 */


import { createAsyncThunk, createSlice, nanoid, PayloadAction } from '@reduxjs/toolkit'
import { client } from '../../api/client'
import { RootState } from '../../app/store'
import { Post, PostState, Reactions } from './types'

// const initialState = [
// 	{ id: '1', title: 'First Post!', content: 'Hello!', user: '1', reactions: {thumbsUp: 0, hooray: 0, heart: 0, rocket: 0, eyes: 0} },
// 	{ id: '2', title: 'Second Post', content: 'More text', user: '3', reactions: {thumbsUp: 1, hooray: 3,  heart: 0, rocket: 0, eyes: 0} }
// ];

// status is an enum that gives information about the loading of posts
// the values of this enum are: idle, loading, complete, failed (can be anything)

const initialState: PostState = {
	posts: [],
	status: 'idle', 
	error: ''
}

export const fetchPosts = createAsyncThunk('posts/fetchPosts', async () => {
	const response = await client.get('fakeApi/posts')
	return response.posts as Post[]
})

export const addNewPost = createAsyncThunk('posts/addNewPost', async (newPost: Post) => {
	const response = await client.post('fakeApi/posts', {post: newPost})
	return response.post as Post
})

const postsSlice = createSlice({
	name: 'posts',
	initialState,
	reducers: {
		postAdded: {
			reducer(state, action: PayloadAction<Post>) {
				state.posts.push(action.payload);
			},
			prepare(title: string, content: string, userId: string) {
				return {
					payload: {
						id: nanoid(),
						title,
						content,
						user: userId,
						reactions: {} as Reactions,
					}
				}
			}
		},
		
		postUpdated: (state, action) => {
			const {id, title, content} = action.payload;

			const oldPost = state.posts.find(post => post.id === id);
			if(oldPost) {
				oldPost.title = title;
				oldPost.content = content;
			}
		},

		reactionAdded: (state, action) => {
			const {postId, reaction} : {postId: string, reaction: keyof Reactions} = action.payload
			const currentPost = state.posts.find(post => post.id === postId) as Post;
			
			if(currentPost) {
				currentPost.reactions[reaction]++;
			}
		}
	},
	extraReducers: builder => {
		builder.addCase(fetchPosts.pending, (state, action) => {
			state.status = 'loading'
		})
		builder.addCase(fetchPosts.fulfilled, (state, action) => {
			state.status = 'complete'
			state.posts = state.posts.concat(action.payload)
		})
		builder.addCase(fetchPosts.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message as string
		})
		builder.addCase(addNewPost.fulfilled, (state, action) => {
			state.posts.push(action.payload)
		})
	}
});
 
export const { postAdded, postUpdated, reactionAdded } = postsSlice.actions;

export const selectAllPosts = (state: RootState) => state.posts.posts as Post[]
export const selectPostById = (state: RootState, postId: string) => state.posts.posts.find(post => post.id === postId) as Post

export default postsSlice.reducer;