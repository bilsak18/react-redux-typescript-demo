/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday November 10th 2020
 */


import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import store from './app/store'
import { Provider } from 'react-redux'

import './api/server'
import { fetchUsers } from './features/users/usersSlice'

store.dispatch(fetchUsers())

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

// expose redux store when run in Cypress
// @ts-ignore
if(window.Cypress) {
	// @ts-ignore
	window.store = store
}
