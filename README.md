# PROJECT DESCRIPTION

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

It was then migrated to Typescript, which is why *./src* files are written Typescript, except for the *./src/api* files which are written in Javascript.

Cypress scripts are written in Javascript and organized in the *./cypress/integration* folder, which is the default folder structure provided and proposed by Cypress.

The mock API is developped using MirageJS library, written with Javascript and organized in the *./src/api* folder.

## Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


### `npx cypress open` or `npm run cy:open`

Opens Cypress Test Runner from where E2E tests can be launched

### `npm run cy:run`

Runs the cypress tests in record mode. They can be visualized on the [Cypress Dashboard](https://dashboard.cypress.io) 

## CI Pipeline with Cypress

A continuous integration pipeline is configured for this project with Gitlab CI through a config file located at the root of the project named *.gitlab-ci.yml* 

The pipeline is configured to run two jobs after every commit on the master branch.

The first job sets up the environment by installing all the dependencies of the project (Cypress included)<br/>
The second job starts the server in the background and runs the Cypress runner to execute the E2E tests in record mode which makes it possible to visualize the reports of those tests on the [Cypress Dashboard](https://dashboard.cypress.io) 